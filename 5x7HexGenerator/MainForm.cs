using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace _x7HexGenerator
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            tbBin0.Text = "0000000";
            tbBin1.Text = "0000000";
            tbBin2.Text = "0000000";
            tbBin3.Text = "0000000";
            tbBin4.Text = "0000000";
            UpdateBin0();
            UpdateBin1();
            UpdateBin2();
            UpdateBin3();
            UpdateBin4();
            UpdateHex();
        }

        #region Hex0
        private void UpdateBin0()
        {
            char[] hex = new char[7];
            if (cb06.Checked) hex[0] = '1'; else hex[0] = '0';
            if (cb05.Checked) hex[1] = '1'; else hex[1] = '0';
            if (cb04.Checked) hex[2] = '1'; else hex[2] = '0';
            if (cb03.Checked) hex[3] = '1'; else hex[3] = '0';
            if (cb02.Checked) hex[4] = '1'; else hex[4] = '0';
            if (cb01.Checked) hex[5] = '1'; else hex[5] = '0';
            if (cb00.Checked) hex[6] = '1'; else hex[6] = '0';
            tbBin0.Text = new String(hex);
            UpdateHex();
        }

        private void cb00_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin0();
        }

        private void cb01_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin0();
        }

        private void cb02_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin0();
        }

        private void cb03_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin0();
        }

        private void cb04_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin0();
        }

        private void cb05_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin0();
        }

        private void cb06_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin0();
        }
        #endregion


        #region Hex1
        private void UpdateBin1()
        {
            char[] hex = new char[7];
            if (cb16.Checked) hex[0] = '1'; else hex[0] = '0';
            if (cb15.Checked) hex[1] = '1'; else hex[1] = '0';
            if (cb14.Checked) hex[2] = '1'; else hex[2] = '0';
            if (cb13.Checked) hex[3] = '1'; else hex[3] = '0';
            if (cb12.Checked) hex[4] = '1'; else hex[4] = '0';
            if (cb11.Checked) hex[5] = '1'; else hex[5] = '0';
            if (cb10.Checked) hex[6] = '1'; else hex[6] = '0';
            tbBin1.Text = new String(hex);
            UpdateHex();
        }

        private void cb10_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin1();
        }

        private void cb11_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin1();
        }

        private void cb12_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin1();
        }

        private void cb13_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin1();
        }

        private void cb14_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin1();
        }

        private void cb15_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin1();
        }

        private void cb16_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin1();
        }
        #endregion


        #region Hex2
        private void UpdateBin2()
        {
            char[] hex = new char[7];
            if (cb26.Checked) hex[0] = '1'; else hex[0] = '0';
            if (cb25.Checked) hex[1] = '1'; else hex[1] = '0';
            if (cb24.Checked) hex[2] = '1'; else hex[2] = '0';
            if (cb23.Checked) hex[3] = '1'; else hex[3] = '0';
            if (cb22.Checked) hex[4] = '1'; else hex[4] = '0';
            if (cb21.Checked) hex[5] = '1'; else hex[5] = '0';
            if (cb20.Checked) hex[6] = '1'; else hex[6] = '0';
            tbBin2.Text = new String(hex);
            UpdateHex();
        }


        

        private void cb20_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin2();
        }

        private void cb21_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin2();
        }

        private void cb22_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin2();
        }

        private void cb23_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin2();
        }

        private void cb24_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin2();
        }

        private void cb25_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin2();
        }

        private void cb26_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin2();
        }

        #endregion


        #region Hex3
        private void UpdateBin3()
        {
            char[] hex = new char[7];
            if (cb36.Checked) hex[0] = '1'; else hex[0] = '0';
            if (cb35.Checked) hex[1] = '1'; else hex[1] = '0';
            if (cb34.Checked) hex[2] = '1'; else hex[2] = '0';
            if (cb33.Checked) hex[3] = '1'; else hex[3] = '0';
            if (cb32.Checked) hex[4] = '1'; else hex[4] = '0';
            if (cb31.Checked) hex[5] = '1'; else hex[5] = '0';
            if (cb30.Checked) hex[6] = '1'; else hex[6] = '0';
            tbBin3.Text = new String(hex);
            UpdateHex();
        }
        

        private void cb30_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin3();
        }

        private void cb31_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin3();
        }

        private void cb32_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin3();
        }

        private void cb33_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin3();
        }

        private void cb34_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin3();
        }

        private void cb35_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin3();
        }

        private void cb36_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin3();
        }
        #endregion


        #region Hex4
        private void UpdateBin4()
        {
            char[] hex = new char[7];
            if (cb46.Checked) hex[0] = '1'; else hex[0] = '0';
            if (cb45.Checked) hex[1] = '1'; else hex[1] = '0';
            if (cb44.Checked) hex[2] = '1'; else hex[2] = '0';
            if (cb43.Checked) hex[3] = '1'; else hex[3] = '0';
            if (cb42.Checked) hex[4] = '1'; else hex[4] = '0';
            if (cb41.Checked) hex[5] = '1'; else hex[5] = '0';
            if (cb40.Checked) hex[6] = '1'; else hex[6] = '0';
            tbBin4.Text = new String(hex);
            UpdateHex();
        }

        private void cb40_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin4();
        }

        private void cb41_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin4();
        }

        private void cb42_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin4();
        }

        private void cb43_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin4();
        }

        private void cb44_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin4();
        }

        private void cb45_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin4();
        }

        private void cb46_CheckedChanged(object sender, EventArgs e)
        {
            UpdateBin4();
        }
        #endregion


        private void UpdateHex()
        {
            String Hex0 = Convert.ToString(Convert.ToInt32(tbBin0.Text, 2), 16);
            String Hex1 = Convert.ToString(Convert.ToInt32(tbBin1.Text, 2), 16);
            String Hex2 = Convert.ToString(Convert.ToInt32(tbBin2.Text, 2), 16);
            String Hex3 = Convert.ToString(Convert.ToInt32(tbBin3.Text, 2), 16);
            String Hex4 = Convert.ToString(Convert.ToInt32(tbBin4.Text, 2), 16);

            if (Hex0.Length == 1) Hex0 = "0" + Hex0;
            if (Hex1.Length == 1) Hex1 = "0" + Hex1;
            if (Hex2.Length == 1) Hex2 = "0" + Hex2;
            if (Hex3.Length == 1) Hex3 = "0" + Hex3;
            if (Hex4.Length == 1) Hex4 = "0" + Hex4;

            tbHex.Text = "0x" + Hex0 + ";\r\n";
            tbHex.Text += "0x" + Hex1 + ";\r\n";
            tbHex.Text += "0x" + Hex2 + ";\r\n";
            tbHex.Text += "0x" + Hex3 + ";\r\n";
            tbHex.Text += "0x" + Hex4 + ";\r\n";
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}