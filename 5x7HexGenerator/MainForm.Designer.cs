namespace _x7HexGenerator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbSegment = new System.Windows.Forms.GroupBox();
            this.cb46 = new System.Windows.Forms.CheckBox();
            this.cb36 = new System.Windows.Forms.CheckBox();
            this.cb26 = new System.Windows.Forms.CheckBox();
            this.cb16 = new System.Windows.Forms.CheckBox();
            this.cb06 = new System.Windows.Forms.CheckBox();
            this.cb45 = new System.Windows.Forms.CheckBox();
            this.cb35 = new System.Windows.Forms.CheckBox();
            this.cb25 = new System.Windows.Forms.CheckBox();
            this.cb15 = new System.Windows.Forms.CheckBox();
            this.cb05 = new System.Windows.Forms.CheckBox();
            this.cb44 = new System.Windows.Forms.CheckBox();
            this.cb34 = new System.Windows.Forms.CheckBox();
            this.cb24 = new System.Windows.Forms.CheckBox();
            this.cb14 = new System.Windows.Forms.CheckBox();
            this.cb04 = new System.Windows.Forms.CheckBox();
            this.cb43 = new System.Windows.Forms.CheckBox();
            this.cb33 = new System.Windows.Forms.CheckBox();
            this.cb23 = new System.Windows.Forms.CheckBox();
            this.cb13 = new System.Windows.Forms.CheckBox();
            this.cb03 = new System.Windows.Forms.CheckBox();
            this.cb42 = new System.Windows.Forms.CheckBox();
            this.cb32 = new System.Windows.Forms.CheckBox();
            this.cb22 = new System.Windows.Forms.CheckBox();
            this.cb12 = new System.Windows.Forms.CheckBox();
            this.cb02 = new System.Windows.Forms.CheckBox();
            this.cb41 = new System.Windows.Forms.CheckBox();
            this.cb31 = new System.Windows.Forms.CheckBox();
            this.cb21 = new System.Windows.Forms.CheckBox();
            this.cb11 = new System.Windows.Forms.CheckBox();
            this.cb01 = new System.Windows.Forms.CheckBox();
            this.cb40 = new System.Windows.Forms.CheckBox();
            this.cb30 = new System.Windows.Forms.CheckBox();
            this.cb20 = new System.Windows.Forms.CheckBox();
            this.cb10 = new System.Windows.Forms.CheckBox();
            this.cb00 = new System.Windows.Forms.CheckBox();
            this.btOK = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbBin4 = new System.Windows.Forms.TextBox();
            this.tbBin3 = new System.Windows.Forms.TextBox();
            this.tbBin2 = new System.Windows.Forms.TextBox();
            this.tbBin1 = new System.Windows.Forms.TextBox();
            this.tbBin0 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbHex = new System.Windows.Forms.TextBox();
            this.gbSegment.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbSegment
            // 
            this.gbSegment.Controls.Add(this.cb46);
            this.gbSegment.Controls.Add(this.cb36);
            this.gbSegment.Controls.Add(this.cb26);
            this.gbSegment.Controls.Add(this.cb16);
            this.gbSegment.Controls.Add(this.cb06);
            this.gbSegment.Controls.Add(this.cb45);
            this.gbSegment.Controls.Add(this.cb35);
            this.gbSegment.Controls.Add(this.cb25);
            this.gbSegment.Controls.Add(this.cb15);
            this.gbSegment.Controls.Add(this.cb05);
            this.gbSegment.Controls.Add(this.cb44);
            this.gbSegment.Controls.Add(this.cb34);
            this.gbSegment.Controls.Add(this.cb24);
            this.gbSegment.Controls.Add(this.cb14);
            this.gbSegment.Controls.Add(this.cb04);
            this.gbSegment.Controls.Add(this.cb43);
            this.gbSegment.Controls.Add(this.cb33);
            this.gbSegment.Controls.Add(this.cb23);
            this.gbSegment.Controls.Add(this.cb13);
            this.gbSegment.Controls.Add(this.cb03);
            this.gbSegment.Controls.Add(this.cb42);
            this.gbSegment.Controls.Add(this.cb32);
            this.gbSegment.Controls.Add(this.cb22);
            this.gbSegment.Controls.Add(this.cb12);
            this.gbSegment.Controls.Add(this.cb02);
            this.gbSegment.Controls.Add(this.cb41);
            this.gbSegment.Controls.Add(this.cb31);
            this.gbSegment.Controls.Add(this.cb21);
            this.gbSegment.Controls.Add(this.cb11);
            this.gbSegment.Controls.Add(this.cb01);
            this.gbSegment.Controls.Add(this.cb40);
            this.gbSegment.Controls.Add(this.cb30);
            this.gbSegment.Controls.Add(this.cb20);
            this.gbSegment.Controls.Add(this.cb10);
            this.gbSegment.Controls.Add(this.cb00);
            this.gbSegment.Location = new System.Drawing.Point(12, 12);
            this.gbSegment.Name = "gbSegment";
            this.gbSegment.Size = new System.Drawing.Size(142, 188);
            this.gbSegment.TabIndex = 35;
            this.gbSegment.TabStop = false;
            this.gbSegment.Text = "LED Diagram";
            // 
            // cb46
            // 
            this.cb46.AutoSize = true;
            this.cb46.Location = new System.Drawing.Point(107, 150);
            this.cb46.Name = "cb46";
            this.cb46.Size = new System.Drawing.Size(15, 14);
            this.cb46.TabIndex = 69;
            this.cb46.UseVisualStyleBackColor = true;
            this.cb46.CheckedChanged += new System.EventHandler(this.cb46_CheckedChanged);
            // 
            // cb36
            // 
            this.cb36.AutoSize = true;
            this.cb36.Location = new System.Drawing.Point(86, 150);
            this.cb36.Name = "cb36";
            this.cb36.Size = new System.Drawing.Size(15, 14);
            this.cb36.TabIndex = 68;
            this.cb36.UseVisualStyleBackColor = true;
            this.cb36.CheckedChanged += new System.EventHandler(this.cb36_CheckedChanged);
            // 
            // cb26
            // 
            this.cb26.AutoSize = true;
            this.cb26.Location = new System.Drawing.Point(65, 150);
            this.cb26.Name = "cb26";
            this.cb26.Size = new System.Drawing.Size(15, 14);
            this.cb26.TabIndex = 67;
            this.cb26.UseVisualStyleBackColor = true;
            this.cb26.CheckedChanged += new System.EventHandler(this.cb26_CheckedChanged);
            // 
            // cb16
            // 
            this.cb16.AutoSize = true;
            this.cb16.Location = new System.Drawing.Point(44, 150);
            this.cb16.Name = "cb16";
            this.cb16.Size = new System.Drawing.Size(15, 14);
            this.cb16.TabIndex = 66;
            this.cb16.UseVisualStyleBackColor = true;
            this.cb16.CheckedChanged += new System.EventHandler(this.cb16_CheckedChanged);
            // 
            // cb06
            // 
            this.cb06.AutoSize = true;
            this.cb06.Location = new System.Drawing.Point(23, 150);
            this.cb06.Name = "cb06";
            this.cb06.Size = new System.Drawing.Size(15, 14);
            this.cb06.TabIndex = 65;
            this.cb06.UseVisualStyleBackColor = true;
            this.cb06.CheckedChanged += new System.EventHandler(this.cb06_CheckedChanged);
            // 
            // cb45
            // 
            this.cb45.AutoSize = true;
            this.cb45.Location = new System.Drawing.Point(107, 130);
            this.cb45.Name = "cb45";
            this.cb45.Size = new System.Drawing.Size(15, 14);
            this.cb45.TabIndex = 64;
            this.cb45.UseVisualStyleBackColor = true;
            this.cb45.CheckedChanged += new System.EventHandler(this.cb45_CheckedChanged);
            // 
            // cb35
            // 
            this.cb35.AutoSize = true;
            this.cb35.Location = new System.Drawing.Point(86, 130);
            this.cb35.Name = "cb35";
            this.cb35.Size = new System.Drawing.Size(15, 14);
            this.cb35.TabIndex = 63;
            this.cb35.UseVisualStyleBackColor = true;
            this.cb35.CheckedChanged += new System.EventHandler(this.cb35_CheckedChanged);
            // 
            // cb25
            // 
            this.cb25.AutoSize = true;
            this.cb25.Location = new System.Drawing.Point(65, 130);
            this.cb25.Name = "cb25";
            this.cb25.Size = new System.Drawing.Size(15, 14);
            this.cb25.TabIndex = 62;
            this.cb25.UseVisualStyleBackColor = true;
            this.cb25.CheckedChanged += new System.EventHandler(this.cb25_CheckedChanged);
            // 
            // cb15
            // 
            this.cb15.AutoSize = true;
            this.cb15.Location = new System.Drawing.Point(44, 130);
            this.cb15.Name = "cb15";
            this.cb15.Size = new System.Drawing.Size(15, 14);
            this.cb15.TabIndex = 61;
            this.cb15.UseVisualStyleBackColor = true;
            this.cb15.CheckedChanged += new System.EventHandler(this.cb15_CheckedChanged);
            // 
            // cb05
            // 
            this.cb05.AutoSize = true;
            this.cb05.Location = new System.Drawing.Point(23, 130);
            this.cb05.Name = "cb05";
            this.cb05.Size = new System.Drawing.Size(15, 14);
            this.cb05.TabIndex = 60;
            this.cb05.UseVisualStyleBackColor = true;
            this.cb05.CheckedChanged += new System.EventHandler(this.cb05_CheckedChanged);
            // 
            // cb44
            // 
            this.cb44.AutoSize = true;
            this.cb44.Location = new System.Drawing.Point(107, 110);
            this.cb44.Name = "cb44";
            this.cb44.Size = new System.Drawing.Size(15, 14);
            this.cb44.TabIndex = 59;
            this.cb44.UseVisualStyleBackColor = true;
            this.cb44.CheckedChanged += new System.EventHandler(this.cb44_CheckedChanged);
            // 
            // cb34
            // 
            this.cb34.AutoSize = true;
            this.cb34.Location = new System.Drawing.Point(86, 110);
            this.cb34.Name = "cb34";
            this.cb34.Size = new System.Drawing.Size(15, 14);
            this.cb34.TabIndex = 58;
            this.cb34.UseVisualStyleBackColor = true;
            this.cb34.CheckedChanged += new System.EventHandler(this.cb34_CheckedChanged);
            // 
            // cb24
            // 
            this.cb24.AutoSize = true;
            this.cb24.Location = new System.Drawing.Point(65, 110);
            this.cb24.Name = "cb24";
            this.cb24.Size = new System.Drawing.Size(15, 14);
            this.cb24.TabIndex = 57;
            this.cb24.UseVisualStyleBackColor = true;
            this.cb24.CheckedChanged += new System.EventHandler(this.cb24_CheckedChanged);
            // 
            // cb14
            // 
            this.cb14.AutoSize = true;
            this.cb14.Location = new System.Drawing.Point(44, 110);
            this.cb14.Name = "cb14";
            this.cb14.Size = new System.Drawing.Size(15, 14);
            this.cb14.TabIndex = 56;
            this.cb14.UseVisualStyleBackColor = true;
            this.cb14.CheckedChanged += new System.EventHandler(this.cb14_CheckedChanged);
            // 
            // cb04
            // 
            this.cb04.AutoSize = true;
            this.cb04.Location = new System.Drawing.Point(23, 110);
            this.cb04.Name = "cb04";
            this.cb04.Size = new System.Drawing.Size(15, 14);
            this.cb04.TabIndex = 55;
            this.cb04.UseVisualStyleBackColor = true;
            this.cb04.CheckedChanged += new System.EventHandler(this.cb04_CheckedChanged);
            // 
            // cb43
            // 
            this.cb43.AutoSize = true;
            this.cb43.Location = new System.Drawing.Point(107, 90);
            this.cb43.Name = "cb43";
            this.cb43.Size = new System.Drawing.Size(15, 14);
            this.cb43.TabIndex = 54;
            this.cb43.UseVisualStyleBackColor = true;
            this.cb43.CheckedChanged += new System.EventHandler(this.cb43_CheckedChanged);
            // 
            // cb33
            // 
            this.cb33.AutoSize = true;
            this.cb33.Location = new System.Drawing.Point(86, 90);
            this.cb33.Name = "cb33";
            this.cb33.Size = new System.Drawing.Size(15, 14);
            this.cb33.TabIndex = 53;
            this.cb33.UseVisualStyleBackColor = true;
            this.cb33.CheckedChanged += new System.EventHandler(this.cb33_CheckedChanged);
            // 
            // cb23
            // 
            this.cb23.AutoSize = true;
            this.cb23.Location = new System.Drawing.Point(65, 90);
            this.cb23.Name = "cb23";
            this.cb23.Size = new System.Drawing.Size(15, 14);
            this.cb23.TabIndex = 52;
            this.cb23.UseVisualStyleBackColor = true;
            this.cb23.CheckedChanged += new System.EventHandler(this.cb23_CheckedChanged);
            // 
            // cb13
            // 
            this.cb13.AutoSize = true;
            this.cb13.Location = new System.Drawing.Point(44, 90);
            this.cb13.Name = "cb13";
            this.cb13.Size = new System.Drawing.Size(15, 14);
            this.cb13.TabIndex = 51;
            this.cb13.UseVisualStyleBackColor = true;
            this.cb13.CheckedChanged += new System.EventHandler(this.cb13_CheckedChanged);
            // 
            // cb03
            // 
            this.cb03.AutoSize = true;
            this.cb03.Location = new System.Drawing.Point(23, 90);
            this.cb03.Name = "cb03";
            this.cb03.Size = new System.Drawing.Size(15, 14);
            this.cb03.TabIndex = 50;
            this.cb03.UseVisualStyleBackColor = true;
            this.cb03.CheckedChanged += new System.EventHandler(this.cb03_CheckedChanged);
            // 
            // cb42
            // 
            this.cb42.AutoSize = true;
            this.cb42.Location = new System.Drawing.Point(107, 70);
            this.cb42.Name = "cb42";
            this.cb42.Size = new System.Drawing.Size(15, 14);
            this.cb42.TabIndex = 49;
            this.cb42.UseVisualStyleBackColor = true;
            this.cb42.CheckedChanged += new System.EventHandler(this.cb42_CheckedChanged);
            // 
            // cb32
            // 
            this.cb32.AutoSize = true;
            this.cb32.Location = new System.Drawing.Point(86, 70);
            this.cb32.Name = "cb32";
            this.cb32.Size = new System.Drawing.Size(15, 14);
            this.cb32.TabIndex = 48;
            this.cb32.UseVisualStyleBackColor = true;
            this.cb32.CheckedChanged += new System.EventHandler(this.cb32_CheckedChanged);
            // 
            // cb22
            // 
            this.cb22.AutoSize = true;
            this.cb22.Location = new System.Drawing.Point(65, 70);
            this.cb22.Name = "cb22";
            this.cb22.Size = new System.Drawing.Size(15, 14);
            this.cb22.TabIndex = 47;
            this.cb22.UseVisualStyleBackColor = true;
            this.cb22.CheckedChanged += new System.EventHandler(this.cb22_CheckedChanged);
            // 
            // cb12
            // 
            this.cb12.AutoSize = true;
            this.cb12.Location = new System.Drawing.Point(44, 70);
            this.cb12.Name = "cb12";
            this.cb12.Size = new System.Drawing.Size(15, 14);
            this.cb12.TabIndex = 46;
            this.cb12.UseVisualStyleBackColor = true;
            this.cb12.CheckedChanged += new System.EventHandler(this.cb12_CheckedChanged);
            // 
            // cb02
            // 
            this.cb02.AutoSize = true;
            this.cb02.Location = new System.Drawing.Point(23, 70);
            this.cb02.Name = "cb02";
            this.cb02.Size = new System.Drawing.Size(15, 14);
            this.cb02.TabIndex = 45;
            this.cb02.UseVisualStyleBackColor = true;
            this.cb02.CheckedChanged += new System.EventHandler(this.cb02_CheckedChanged);
            // 
            // cb41
            // 
            this.cb41.AutoSize = true;
            this.cb41.Location = new System.Drawing.Point(107, 50);
            this.cb41.Name = "cb41";
            this.cb41.Size = new System.Drawing.Size(15, 14);
            this.cb41.TabIndex = 44;
            this.cb41.UseVisualStyleBackColor = true;
            this.cb41.CheckedChanged += new System.EventHandler(this.cb41_CheckedChanged);
            // 
            // cb31
            // 
            this.cb31.AutoSize = true;
            this.cb31.Location = new System.Drawing.Point(86, 50);
            this.cb31.Name = "cb31";
            this.cb31.Size = new System.Drawing.Size(15, 14);
            this.cb31.TabIndex = 43;
            this.cb31.UseVisualStyleBackColor = true;
            this.cb31.CheckedChanged += new System.EventHandler(this.cb31_CheckedChanged);
            // 
            // cb21
            // 
            this.cb21.AutoSize = true;
            this.cb21.Location = new System.Drawing.Point(65, 50);
            this.cb21.Name = "cb21";
            this.cb21.Size = new System.Drawing.Size(15, 14);
            this.cb21.TabIndex = 42;
            this.cb21.UseVisualStyleBackColor = true;
            this.cb21.CheckedChanged += new System.EventHandler(this.cb21_CheckedChanged);
            // 
            // cb11
            // 
            this.cb11.AutoSize = true;
            this.cb11.Location = new System.Drawing.Point(44, 50);
            this.cb11.Name = "cb11";
            this.cb11.Size = new System.Drawing.Size(15, 14);
            this.cb11.TabIndex = 41;
            this.cb11.UseVisualStyleBackColor = true;
            this.cb11.CheckedChanged += new System.EventHandler(this.cb11_CheckedChanged);
            // 
            // cb01
            // 
            this.cb01.AutoSize = true;
            this.cb01.Location = new System.Drawing.Point(23, 50);
            this.cb01.Name = "cb01";
            this.cb01.Size = new System.Drawing.Size(15, 14);
            this.cb01.TabIndex = 40;
            this.cb01.UseVisualStyleBackColor = true;
            this.cb01.CheckedChanged += new System.EventHandler(this.cb01_CheckedChanged);
            // 
            // cb40
            // 
            this.cb40.AutoSize = true;
            this.cb40.Location = new System.Drawing.Point(107, 30);
            this.cb40.Name = "cb40";
            this.cb40.Size = new System.Drawing.Size(15, 14);
            this.cb40.TabIndex = 39;
            this.cb40.UseVisualStyleBackColor = true;
            this.cb40.CheckedChanged += new System.EventHandler(this.cb40_CheckedChanged);
            // 
            // cb30
            // 
            this.cb30.AutoSize = true;
            this.cb30.Location = new System.Drawing.Point(86, 30);
            this.cb30.Name = "cb30";
            this.cb30.Size = new System.Drawing.Size(15, 14);
            this.cb30.TabIndex = 38;
            this.cb30.UseVisualStyleBackColor = true;
            this.cb30.CheckedChanged += new System.EventHandler(this.cb30_CheckedChanged);
            // 
            // cb20
            // 
            this.cb20.AutoSize = true;
            this.cb20.BackColor = System.Drawing.SystemColors.Control;
            this.cb20.Location = new System.Drawing.Point(65, 30);
            this.cb20.Name = "cb20";
            this.cb20.Size = new System.Drawing.Size(15, 14);
            this.cb20.TabIndex = 37;
            this.cb20.UseVisualStyleBackColor = false;
            this.cb20.CheckedChanged += new System.EventHandler(this.cb20_CheckedChanged);
            // 
            // cb10
            // 
            this.cb10.AutoSize = true;
            this.cb10.Location = new System.Drawing.Point(44, 30);
            this.cb10.Name = "cb10";
            this.cb10.Size = new System.Drawing.Size(15, 14);
            this.cb10.TabIndex = 36;
            this.cb10.UseVisualStyleBackColor = true;
            this.cb10.CheckedChanged += new System.EventHandler(this.cb10_CheckedChanged);
            // 
            // cb00
            // 
            this.cb00.AutoSize = true;
            this.cb00.Location = new System.Drawing.Point(23, 30);
            this.cb00.Name = "cb00";
            this.cb00.Size = new System.Drawing.Size(15, 14);
            this.cb00.TabIndex = 35;
            this.cb00.UseVisualStyleBackColor = true;
            this.cb00.CheckedChanged += new System.EventHandler(this.cb00_CheckedChanged);
            // 
            // btOK
            // 
            this.btOK.Location = new System.Drawing.Point(387, 208);
            this.btOK.Name = "btOK";
            this.btOK.Size = new System.Drawing.Size(75, 23);
            this.btOK.TabIndex = 36;
            this.btOK.Text = "OK";
            this.btOK.UseVisualStyleBackColor = true;
            this.btOK.Click += new System.EventHandler(this.btOK_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbBin4);
            this.groupBox1.Controls.Add(this.tbBin3);
            this.groupBox1.Controls.Add(this.tbBin2);
            this.groupBox1.Controls.Add(this.tbBin1);
            this.groupBox1.Controls.Add(this.tbBin0);
            this.groupBox1.Location = new System.Drawing.Point(160, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(146, 188);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Binary";
            // 
            // tbBin4
            // 
            this.tbBin4.Location = new System.Drawing.Point(23, 142);
            this.tbBin4.Name = "tbBin4";
            this.tbBin4.Size = new System.Drawing.Size(100, 21);
            this.tbBin4.TabIndex = 4;
            // 
            // tbBin3
            // 
            this.tbBin3.Location = new System.Drawing.Point(23, 114);
            this.tbBin3.Name = "tbBin3";
            this.tbBin3.Size = new System.Drawing.Size(100, 21);
            this.tbBin3.TabIndex = 3;
            // 
            // tbBin2
            // 
            this.tbBin2.Location = new System.Drawing.Point(23, 86);
            this.tbBin2.Name = "tbBin2";
            this.tbBin2.Size = new System.Drawing.Size(100, 21);
            this.tbBin2.TabIndex = 2;
            // 
            // tbBin1
            // 
            this.tbBin1.Location = new System.Drawing.Point(23, 58);
            this.tbBin1.Name = "tbBin1";
            this.tbBin1.Size = new System.Drawing.Size(100, 21);
            this.tbBin1.TabIndex = 1;
            // 
            // tbBin0
            // 
            this.tbBin0.Location = new System.Drawing.Point(23, 30);
            this.tbBin0.Name = "tbBin0";
            this.tbBin0.Size = new System.Drawing.Size(100, 21);
            this.tbBin0.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbHex);
            this.groupBox2.Location = new System.Drawing.Point(312, 14);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(146, 188);
            this.groupBox2.TabIndex = 38;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Hex";
            // 
            // tbHex
            // 
            this.tbHex.Location = new System.Drawing.Point(7, 28);
            this.tbHex.Multiline = true;
            this.tbHex.Name = "tbHex";
            this.tbHex.Size = new System.Drawing.Size(133, 133);
            this.tbHex.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 243);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btOK);
            this.Controls.Add(this.gbSegment);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "MainForm";
            this.Text = "5x7 Hex Generator";
            this.gbSegment.ResumeLayout(false);
            this.gbSegment.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbSegment;
        private System.Windows.Forms.CheckBox cb46;
        private System.Windows.Forms.CheckBox cb36;
        private System.Windows.Forms.CheckBox cb26;
        private System.Windows.Forms.CheckBox cb16;
        private System.Windows.Forms.CheckBox cb06;
        private System.Windows.Forms.CheckBox cb45;
        private System.Windows.Forms.CheckBox cb35;
        private System.Windows.Forms.CheckBox cb25;
        private System.Windows.Forms.CheckBox cb15;
        private System.Windows.Forms.CheckBox cb05;
        private System.Windows.Forms.CheckBox cb44;
        private System.Windows.Forms.CheckBox cb34;
        private System.Windows.Forms.CheckBox cb24;
        private System.Windows.Forms.CheckBox cb14;
        private System.Windows.Forms.CheckBox cb04;
        private System.Windows.Forms.CheckBox cb43;
        private System.Windows.Forms.CheckBox cb33;
        private System.Windows.Forms.CheckBox cb23;
        private System.Windows.Forms.CheckBox cb13;
        private System.Windows.Forms.CheckBox cb03;
        private System.Windows.Forms.CheckBox cb42;
        private System.Windows.Forms.CheckBox cb32;
        private System.Windows.Forms.CheckBox cb22;
        private System.Windows.Forms.CheckBox cb12;
        private System.Windows.Forms.CheckBox cb02;
        private System.Windows.Forms.CheckBox cb41;
        private System.Windows.Forms.CheckBox cb31;
        private System.Windows.Forms.CheckBox cb21;
        private System.Windows.Forms.CheckBox cb11;
        private System.Windows.Forms.CheckBox cb01;
        private System.Windows.Forms.CheckBox cb40;
        private System.Windows.Forms.CheckBox cb30;
        private System.Windows.Forms.CheckBox cb20;
        private System.Windows.Forms.CheckBox cb10;
        private System.Windows.Forms.CheckBox cb00;
        private System.Windows.Forms.Button btOK;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbBin4;
        private System.Windows.Forms.TextBox tbBin3;
        private System.Windows.Forms.TextBox tbBin2;
        private System.Windows.Forms.TextBox tbBin1;
        private System.Windows.Forms.TextBox tbBin0;
        private System.Windows.Forms.TextBox tbHex;
    }
}

